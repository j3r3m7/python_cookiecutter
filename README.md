# Cookie cutter python project

This repo is used with the cookie cutter tool as a timesaver device for creating all the bits required in a well set python project in one go.
It avoids the tedious admin of having to make tests folder, src folder, poetry setup etc.

## Usage

First install the cookiecutter tool using:

```
pip install cookiecutter
```

Once the tool is installed then run the command:

```
cookiecutter git@gitlab.com:berniecamus/python-template.git
```
